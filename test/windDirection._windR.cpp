/*
  Analog Input
 Demonstrates analog input by reading an analog sensor on analog pin 0 and
 turning on and off a light emitting diode(LED)  connected to digital pin 13. 
 The amount of time the LED will be on and off depends on
 the value obtained by analogRead(). 
 
 The circuit:
 * Potentiometer attached to analog input 0
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * LED anode (long leg) attached to digital output 13
 * LED cathode (short leg) attached to ground
 
 * Note: because most Arduinos have a built-in LED attached 
 to pin 13 on the board, the LED is optional.
 
 
 Created by David Cuartielles
 modified 30 Aug 2011
 By Tom Igoe
 
 This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/AnalogInput
 
 */
 
#include <Arduino.h>
int sensorWindRichtung = A2;    // select the input pin for the potentiometer
int ledPin = 13;      // select the pin for the LED
int i;
int azimutHall;
int count = 0;
String wr;

/*
  // Pin 13: Arduino has an LED connected on pin 13
  // Pin 11: Teensy 2.0 has the LED on pin 11
  // Pin  6: Teensy++ 2.0 has the LED on pin 6
  // Pin 13: Teensy 3.0 has the LED on pin 13
  // Pin 13: Teensy 3.2 has the LED on pin 13

int azimutPin = A0;
 =============================================================
*/


int sensorValue = 0;  // variable to store the value coming from the sensor


int Windrichtung(float azimutHall) {
    int i;
    if(azimutHall >= 0 && azimutHall < 11) { i = 0; }
    if(azimutHall >= 11 && azimutHall < 34) { i = 1; }
    if(azimutHall >= 34 && azimutHall < 56) { i = 2; }
    if(azimutHall >= 56 && azimutHall < 79) { i = 3; }
    if(azimutHall >= 79 && azimutHall < 101) { i = 4; }
    if(azimutHall >= 101 && azimutHall < 124) { i = 5; }
    if(azimutHall >= 124 && azimutHall < 146) { i = 6; }
    if(azimutHall >= 146 && azimutHall < 169) { i = 7; }
    if(azimutHall >= 169 && azimutHall < 191) { i = 8; }
    if(azimutHall >= 191 && azimutHall < 214) { i = 9; }
    if(azimutHall >= 214 && azimutHall < 236) { i = 10; }
    if(azimutHall >= 236 && azimutHall < 259) { i = 11; }
    if(azimutHall >= 259 && azimutHall < 281) { i = 12; }
    if(azimutHall >= 281 && azimutHall < 304) { i = 13; }
    if(azimutHall >= 304 && azimutHall < 326) { i = 14; }
    if(azimutHall >= 326 && azimutHall < 349) { i = 15; }
    return i;
}




void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);  
    Serial.begin(115200);         // Init serial port and set baudrate

}

void loop() {
  // read the value from the sensor:
  sensorValue = analogRead(sensorWindRichtung);
  
  delay( 400);
  Serial.begin(115200);         // Init serial port and set baudrate
  Serial.print("WindRichtung : ");
  Serial.println(sensorValue);
   digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second


 const char *windrichtung[16] = {"N  ", "NNO", "NO ", "ONO", "O  ", "OSO", "SO ", "SSO", "S  ", "SSW", "SW ", "WSW", "W  ", "WNW", "NW", "NNW"};

    int sensorAzimValue = analogRead(sensorWindRichtung);
 
      if(sensorAzimValue >= 0 && sensorAzimValue < 512) {
        // Skalierung der Hall-Sensorwerte in Gradwerte 45-180 Grad
        azimutHall = map(sensorAzimValue, 0, 512, 0, 180);
    }

    if(sensorAzimValue >= 506 && sensorAzimValue <= 1023) {
        // Skalierung der Hall-Sensorwerte in Gradwerte
        // 180-359 Grad
        azimutHall = map(sensorAzimValue, 506, 1023, 180, 359);
        // Aufruf Funktion: Windrichtung
        //        Orig
        //    i = Windrichtung(azimutHall);
    }
    
    i = Windrichtung(azimutHall); // Aufruf Funktion: Windrichtung

    wr = windrichtung[i];



}