// CHAPTER: Includes
// #include <Timezone.h>
#include <SPI.h>
#include <lmic.h>
//#include <Wire.h>
#include <Arduino.h>
#include <hal/hal.h>
#include <TimeLib.h>
#include "Adafruit_HTU31D.h"

// CHAPTER: Global
int unsigned LoopCount = 0;
const unsigned int MaxMessageLength = 80; // Wird in Serial.read verwendet
float Sensor;
long TimerJoin = 220000;
long TimeoutJoin = 240000;

long Timer = 0;
long Timeout = 5000;

// Zeit
bool clockHasBeenSet = false;
// (Negative number for moving time backwards (e.g. from GMT to ET)
const int OFFSET_HRS = -1;

// CHAPTER: Pushbutton
#include <Bounce2.h>
#define ButtonPin 23
Bounce2::Button button = Bounce2::Button();

// CHAPTER: LED
#define WhiteLED 22

// CHAPTER: HTU31D Humidity and temperature sensor
Adafruit_HTU31D htu = Adafruit_HTU31D();
uint32_t timestamp;
bool heaterEnabled = false;
// float myTemperature;
// float myHumidity;
// PAYLOAD Initialisieren
// byte payload[2];
// uint8_t message[50];

// CHAPTER: LoRa
// clang-format off
// Format: little-endian
static const u1_t PROGMEM APPEUI[8] = {0xD8, 0xDF, 0x4E, 0xE8, 0xEA, 0xF8, 0x09, 0x35};
void os_getArtEui(u1_t *buf) {
    memcpy_P(buf, APPEUI, 8);
}

// Format: little-endian
static const u1_t PROGMEM DEVEUI[8] = {0x32, 0x73, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70};
void os_getDevEui(u1_t *buf) {
    memcpy_P(buf, DEVEUI, 8);
}

// Format big-endian
static const u1_t PROGMEM APPKEY[16] = {0xA3, 0x6D, 0xB4, 0x4A, 0xE6, 0xC5, 0xCF, 0xED, 0x30, 0x89, 0x7C, 0x97, 0x44, 0xAB, 0x03, 0x1A};
void os_getDevKey(u1_t *buf) {
    memcpy_P(buf, APPKEY, 16);
}
// clang-format on

// PAYLOAD sendjob
static osjob_t sendjob;
const unsigned TX_INTERVAL = 20;

const lmic_pinmap lmic_pins = {
    .nss = 10,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {4, 5, 6},
    .rxtx_rx_active = 0,
    .rssi_cal = 0,
    .spi_freq = 0,
};

void BlinkSlow() {
    for(int i = 0; i <= 5; i++) {
        digitalWrite(WhiteLED, HIGH);
        delay(350);
        digitalWrite(WhiteLED, LOW);
        delay(350);
    }
}

void BlinkFast() {
    for(int i = 0; i <= 50; i++) {
        digitalWrite(WhiteLED, HIGH);
        delay(5);
        digitalWrite(WhiteLED, LOW);
        delay(60);
    }
}

// CHAPTER: LoRa printHex2
void printHex2(unsigned v) {
    v &= 0xff;
    if(v < 16) Serial.print('0');
    Serial.print(v, HEX);
}

// CHAPTER: LoRa do_send
void do_send(osjob_t *j) {
    sensors_event_t humidity, temp;
    htu.getEvent(&humidity, &temp);
    // char ValueT[5];
    // char ValueH[5];
    int Temperature;
    int Humidity;
    // Get Temperature
    Temperature = int(temp.temperature * 10);
    // Get humidity
    Humidity = int(humidity.relative_humidity * 10);

    // Check if there is not a current TX/RX job running
    if(LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // PAYLOAD senden
        Serial.println("PayLoad wird gesendet");
        // prepare and schedule data for transmission
        LMIC.frame[0] = Temperature >> 8;
        LMIC.frame[1] = Temperature;
        // LMIC.frame[2] = Humidity >> 8;
        // LMIC.frame[3] = Humidity;
        Serial.println("");
        Serial.print("frame[0]: ");
        Serial.println(LMIC.frame[0]);
        Serial.print("frame[1]: ");
        Serial.println(LMIC.frame[1]);
        // Serial.print("frame[2]: ");
        // Serial.println(LMIC.frame[2]);
        // Serial.print("frame[3]: ");
        // Serial.println(LMIC.frame[3]);
        Serial.print("Temperatur = ");
        Serial.println(temp.temperature);

        LMIC_setTxData2(1, LMIC.frame, 2,
                        0); // (port 1, 4 bytes, unconfirmed)
        // LMIC_setTxData2(1, payload, sizeof(payload) - 1, 0); // Original
        // LMIC_setTxData2(1, myData, sizeof(myData) - 1, 0); // Original
        // LMIC_setTxData2(1, mydata, sizeof(mydata) - 1, 0); // Original
        //             (port, data, dlen, confirmed)
        // LMIC_setTxData2(1, PayLoad, sizeof(3) - 1, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

// CHAPTER: LoRa onEvent
void onEvent(ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
    case EV_SCAN_TIMEOUT:
        Serial.println(F("EV_SCAN_TIMEOUT"));
        break;
    case EV_BEACON_FOUND:
        Serial.println(F("EV_BEACON_FOUND"));
        break;
    case EV_BEACON_MISSED:
        Serial.println(F("EV_BEACON_MISSED"));
        break;
    case EV_BEACON_TRACKED:
        Serial.println(F("EV_BEACON_TRACKED"));
        break;
    case EV_JOINING:
        LMIC.dn2Dr = DR_SF12;         // So geht es mit einem fremden Gateway
        LMIC_setDrTxpow(DR_SF11, 14); // So geht es mit einem fremden Gateway
        Serial.println(F("EV_JOINING"));
        break;
    // GPS-ABRUF: Join -> Ja
    case EV_JOINED:
        Serial.println(F("EV_JOINED"));
        BlinkSlow();
        {
            u4_t netid = 0;
            devaddr_t devaddr = 0;
            u1_t nwkKey[16];
            u1_t artKey[16];
            LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
            Serial.print("netid: ");
            Serial.println(netid, DEC);
            Serial.print("devaddr: ");
            Serial.println(devaddr, HEX);
            Serial.print("AppSKey: ");
            for(size_t i = 0; i < sizeof(artKey); ++i) {
                if(i != 0) Serial.print("-");
                printHex2(artKey[i]);
            }
            Serial.println("");
            Serial.print("NwkSKey: ");
            for(size_t i = 0; i < sizeof(nwkKey); ++i) {
                if(i != 0) Serial.print("-");
                printHex2(nwkKey[i]);
            }
            Serial.println();
        }
        // Disable link check validation (automatically enabled
        // during join, but because slow data rates change max TX
        // size, we don't use it in this example.
        LMIC_setLinkCheckMode(0);
        LMIC_setAdrMode(0);
        break;
    /*
    || This event is defined but not used in the code. No
    || point in wasting codespace on it.
    ||
    || case EV_RFU1:
    ||     Serial.println(F("EV_RFU1"));
    ||     break;
    */
    case EV_JOIN_FAILED:
        Serial.println(F("EV_JOIN_FAILED"));
        break;
    case EV_REJOIN_FAILED:
        Serial.println(F("EV_REJOIN_FAILED"));
        break;
    case EV_TXCOMPLETE:
        Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
        if(LMIC.txrxFlags & TXRX_ACK) Serial.println(F("Received ack"));
        if(LMIC.dataLen) {
            Serial.print(F("Received "));
            Serial.print(LMIC.dataLen);
            Serial.println(F(" bytes of payload"));
        }
        // Schedule next transmission
        os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL),
                            do_send);
        break;
    case EV_LOST_TSYNC:
        Serial.println(F("EV_LOST_TSYNC"));
        break;
    case EV_RESET:
        Serial.println(F("EV_RESET"));
        break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        Serial.println(F("EV_RXCOMPLETE"));
        break;
    case EV_LINK_DEAD:
        Serial.println(F("EV_LINK_DEAD"));
        break;
    case EV_LINK_ALIVE:
        Serial.println(F("EV_LINK_ALIVE"));
        break;
    /*
    || This event is defined but not used in the code. No
    || point in wasting codespace on it.
    ||
    || case EV_SCAN_FOUND:
    ||    Serial.println(F("EV_SCAN_FOUND"));
    ||    break;
    */
    case EV_TXSTART:
        Serial.println(F("EV_TXSTART"));
        break;
    case EV_TXCANCELED:
        Serial.println(F("EV_TXCANCELED"));
        break;
    case EV_RXSTART:
        /* do not print anything -- it wrecks timing */
        break;
    // GPS-ABRUF: Join -> Nein
    case EV_JOIN_TXCOMPLETE:
        BlinkFast();
        Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
        break;
    default:
        Serial.print(F("Unknown event: "));
        Serial.println((unsigned)ev);
        break;
    }
}

// CHAPTER: JoinVersuch
void JoinVersuch() {
    os_init();
    LMIC_reset();
    LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);
    LMIC_setLinkCheckMode(0);
    LMIC_setAdrMode(1);

    digitalWrite(WhiteLED, HIGH);
    delay(950);
    digitalWrite(WhiteLED, LOW);
    Serial.println("Erneuter Verbindugsversuch");
    do_send(&sendjob);
}

// STUB: Setup
void setup() {
    // SETUP: SD Card
    // Open serial communications and wait for port to open:
    // Serial.begin(9600);
    // wait for Serial Monitor to connect. Needed for native USB port boards
    // only:
    // while(!Serial)
    // ;

    // SETUP: Pushbutton
    button.attach(ButtonPin, INPUT_PULLUP);
    // pinMode(SW1, INPUT_PULLUP);
    // DEBOUNCE INTERVAL IN MILLISECONDS
    button.interval(5);
    // INDICATE THAT THE LOW STATE CORRESPONDS TO PHYSICALLY PRESSING THE
    // BUTTON
    button.setPressedState(LOW);

    // SETUP: LED
    pinMode(WhiteLED, OUTPUT);

    // Setup Serial
    while(!Serial && millis() < 8000) { ; }
    Serial.begin(115200);

    // SETUP: HTU31D
    Serial.println("Adafruit HTU31D test");
    if(!htu.begin(0x40)) {
        Serial.println("Couldn't find sensor!");
        while(1)
            ;
    }
    timestamp = millis();

    // SETUP: LoRa
    Serial.println(F("Start LoRa OTAA"));
    os_init();
    LMIC_reset();
    LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);
    LMIC_setLinkCheckMode(0);
    LMIC_setAdrMode(1);
    LMIC.dn2Dr = DR_SF12;         // So geht es mit einem fremden Gateway
    LMIC_setDrTxpow(DR_SF11, 14); // So geht es mit einem fremden Gateway
    digitalWrite(WhiteLED, HIGH);
    delay(950);
    digitalWrite(WhiteLED, LOW);
    do_send(&sendjob);
    Sensor = random(1, 500);
}
// SETUP: Setup end

// STUB: loop
void loop() {
    int incomingByte = 0;
    if(millis() > TimeoutJoin + TimerJoin) {
        TimerJoin = millis();
        JoinVersuch();
    }

    if(millis() > Timeout + Timer) {
        Timer = millis();
        // Serial.print("clockHasBeenSet: ");
        // Serial.println(clockHasBeenSet);
    }

    // LOOP: Button
    button.update();
    if(button.pressed()) {}

    // LOOP: Serial Input
    if(Serial.available()) { // if there is data comming
        // String command = Serial.readStringUntil('\n'); // read string
        // until meet newline character
        incomingByte = Serial.read();
        // say what you got:
        // Serial.print("I received: ");
        // Serial.println(incomingByte, DEC);

        switch(incomingByte) {
        case 48: // Taste "0" = RampMode 0
            break;
        case 49: // Taste "1" = RampMode 1
            break;
        case 50: // Taste "2" = RampMode 2
            break;
        case 51: // Taste "3" = RampMode 3
            break;
        case 101: // Taste "E" = Einschalten
            Serial.println("Taste E gedrückt");
            BlinkSlow();
            break;
        case 97: // Taste "A" = Ausschalten
            break;
        case 107: // Taste "K" = Kill
            digitalWrite(WhiteLED, LOW);
            BlinkFast();
            break;
        case 105: // Taste "I" = Init
            break;
        case 114: // Taste "r" = re-join
            digitalWrite(WhiteLED, LOW);
            JoinVersuch();
            break;
        case 82: // Taste "R" = re-join
            digitalWrite(WhiteLED, LOW);
            JoinVersuch();
            break;
        case 112: // Taste "p" = print
            Serial.println("Aufruf von: do_send(&sendjob)");
            do_send(&sendjob);
            break;
        case 43: // Taste + = Schneller
            break;
        case 45: // Taste - = Langsamer
            break;
        default:
            // Tue etwas, im Defaultfall
            // Dieser Fall ist optional
            break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
        }
    }
    // LOOP Lora
    os_runloop_once();
}